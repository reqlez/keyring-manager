Keyring-Manager
---------------

Keyring-Manager is a fork of KVDB that has been modified to meet the needs of the Veilid project.

**Keyring-Manager and all files in it are copyright:**

Copyright 2021 Veilid Foundation Inc

Keyring Developers: "Walther Chen <walther.chen@gmail.com>"

**Keyring-Manager is licensed:**

Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
<LICENSE-MIT or http://opensource.org/licenses/MIT>, at your option.
