use crate::*;
use crate::error::{KeyringError, Result};
use core_foundation::base::TCFType;
use core_foundation::boolean::CFBoolean;
use core_foundation::data::CFData;
use core_foundation::dictionary::CFDictionary;
use core_foundation::string::{CFString, CFStringRef};
use core_foundation_sys::base::{CFGetTypeID, OSStatus};
use security_framework_sys::base::errSecSuccess;
use security_framework_sys::item::*;
use security_framework_sys::keychain_item::{SecItemAdd, SecItemCopyMatching, SecItemDelete};
use std::ptr;

#[link(name = "Security", kind = "framework")]
extern "C" {
    pub(crate) static kSecClass: CFStringRef;
    pub(crate) static kSecClassGenericPassword: CFStringRef;
    pub(crate) static kSecAttrAccount: CFStringRef;
    pub(crate) static kSecValueData: CFStringRef;
    pub(crate) static kSecMatchLimitOne: CFStringRef;
}
#[allow(non_upper_case_globals)]
const errSecItemNotFound: OSStatus = -25300;

#[allow(non_upper_case_globals)]
#[inline(always)]
fn cvt(err: OSStatus) -> Result<()> {
    match err {
        errSecSuccess => Ok(()),
        errSecItemNotFound => Err(KeyringError::NoPasswordFound),
        err => Err(KeyringError::from(err)),
    }
}

pub struct IosKeyringManager {
    application: String,
}

impl IosKeyringManager {
    pub fn new(application: &str) -> Result<Self> {
        Ok(IosKeyringManager {
            application: application.to_owned(),
        })
    }

    pub fn with_keyring<F, T>(&self, service: &str, key: &str, func: F) -> Result<T>
    where
        F: FnOnce(&mut dyn Keyring) -> Result<T>,
    {
        let mut kr = IosKeyring::new(&self.application, service, key)?;
        func(&mut kr)
    }
}

pub struct IosKeyring<'a> {
    application: &'a str,
    service: &'a str,
    key: &'a str,
}

impl<'a> IosKeyring<'a> {
    fn new(application: &'a str, service: &'a str, key: &'a str) -> Result<IosKeyring<'a>> {
        Ok(IosKeyring {
            application,
            service,
            key,
        })
    }

    fn make_key_string(&self) -> String {
        [
            snailquote::escape(self.application),
            snailquote::escape(self.service),
            snailquote::escape(self.key),
        ]
        .join("\t")
    }
}
impl<'a> Keyring for IosKeyring<'a> {
    fn set_value(&mut self, value: &str) -> Result<()> {
        let account_name = self.make_key_string();

        let mut params = vec![];
        params.push((
            unsafe { CFString::wrap_under_get_rule(kSecClass).into_CFType() },
            unsafe { CFString::wrap_under_get_rule(kSecClassGenericPassword).into_CFType() },
        ));
        params.push((
            unsafe { CFString::wrap_under_get_rule(kSecAttrAccount).into_CFType() },
            CFString::new(&account_name).into_CFType(),
        ));
        params.push((
            unsafe { CFString::wrap_under_get_rule(kSecValueData).into_CFType() },
            CFData::from_buffer(value.as_bytes()).into_CFType(),
        ));
        let param_dict = CFDictionary::from_CFType_pairs(&params);
        let _ = unsafe { SecItemDelete(param_dict.as_concrete_TypeRef()) };
        let mut ret = ptr::null();
        cvt(unsafe { SecItemAdd(param_dict.as_concrete_TypeRef(), &mut ret) })
    }

    fn get_value(&self) -> Result<String> {
        let account_name = self.make_key_string();

        let mut params = vec![];
        params.push((
            unsafe { CFString::wrap_under_get_rule(kSecClass).into_CFType() },
            unsafe { CFString::wrap_under_get_rule(kSecClassGenericPassword).into_CFType() },
        ));
        params.push((
            unsafe { CFString::wrap_under_get_rule(kSecAttrAccount).into_CFType() },
            CFString::new(&account_name).into_CFType(),
        ));
        params.push((
            unsafe { CFString::wrap_under_get_rule(kSecReturnData).into_CFType() },
            CFBoolean::true_value().into_CFType(),
        ));
        params.push((
            unsafe { CFString::wrap_under_get_rule(kSecMatchLimit).into_CFType() },
            unsafe { CFString::wrap_under_get_rule(kSecMatchLimitOne).into_CFType() },
        ));
        let param_dict = CFDictionary::from_CFType_pairs(&params);

        let mut item = ptr::null();
        cvt(unsafe { SecItemCopyMatching(param_dict.as_concrete_TypeRef(), &mut item) })?;
        let type_id = unsafe { CFGetTypeID(item) };
        if type_id != CFData::type_id() {
            return Err(KeyringError::from(format!(
                "keychain item has wrong type: {}",
                type_id
            )));
        }
        let data = unsafe { CFData::wrap_under_get_rule(item as *mut _) };
        let mut buf = Vec::new();
        buf.extend_from_slice(data.bytes());
        String::from_utf8(buf).map_err(|e| {
            KeyringError::from(format!("couldn't convert keychain data to string: {}", e))
        })
    }

    fn delete_value(&mut self) -> Result<()> {
        let account_name = self.make_key_string();

        let mut params = vec![];
        params.push((
            unsafe { CFString::wrap_under_get_rule(kSecClass).into_CFType() },
            unsafe { CFString::wrap_under_get_rule(kSecClassGenericPassword).into_CFType() },
        ));
        params.push((
            unsafe { CFString::wrap_under_get_rule(kSecAttrAccount).into_CFType() },
            CFString::new(&account_name).into_CFType(),
        ));
        let param_dict = CFDictionary::from_CFType_pairs(&params);

        cvt(unsafe { SecItemDelete(param_dict.as_concrete_TypeRef()) })
    }
}
