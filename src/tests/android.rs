use crate::tests::*;
use android_logger::{Config, FilterBuilder};
use backtrace::Backtrace;
use jni::errors::Result as JniResult;
use jni::{objects::GlobalRef, objects::JClass, objects::JObject, objects::JString, JNIEnv};
use log::*;
use std::panic;
use std::path::{Path, PathBuf};

#[no_mangle]
#[allow(non_snake_case)]
pub extern "system" fn Java_com_keyring_1manager_keyring_1android_1test_MainActivity_run_1tests(
    env: JNIEnv,
    _class: JClass,
    ctx: JObject,
) {
    android_logger::init_once(
        Config::default()
            .with_max_level(LevelFilter::Trace)
            .with_tag("keyring_manager_android_tests")
            .with_filter(FilterBuilder::new().parse("keyring=trace").build()),
    );

    panic::set_hook(Box::new(|panic_info| {
        if let Some(s) = panic_info.payload().downcast_ref::<&str>() {
            println!("panic occurred: {:?}", s);
        } else {
            println!("panic occurred");
        }
        let bt = Backtrace::new();
        println!("Backtrace:\n{:?}", bt);
    }));

    let ctx = env.new_global_ref(ctx).unwrap();
    run_tests(env, ctx);
}

pub fn run_tests(jnienv: JNIEnv, ctx: GlobalRef) {
    info!("Starting unit tests");

    // Test in new non-dalvik native thread to see what happens
    let vm = jnienv.get_java_vm().unwrap();
    std::thread::spawn(move || {
        let mut jnienv = vm.attach_current_thread().unwrap();

        let insecure_keyring_path_string = jnienv
            .with_local_frame(64, |jnienv| {
                // context.getFilesDir().getAbsolutePath()
                let file = jnienv
                    .call_method(ctx.as_obj(), "getFilesDir", "()Ljava/io/File;", &[])
                    .unwrap()
                    .l()
                    .unwrap();
                let path = jnienv
                    .call_method(file, "getAbsolutePath", "()Ljava/lang/String;", &[])
                    .unwrap()
                    .l()
                    .unwrap();

                let jstr = JString::from(path);
                let jstrval = jnienv.get_string(&jstr).unwrap();
                JniResult::Ok(String::from(jstrval.to_string_lossy()))
            })
            .unwrap();
        let insecure_keyring_path: PathBuf =
            Path::new(&insecure_keyring_path_string).join("insecure_keyring");

        let android_context = (jnienv.get_java_vm().unwrap(), ctx.clone());

        let platform_keyring_manager =
            KeyringManager::new_secure(TEST_APPLICATION, android_context).unwrap();
        let insecure_keyring_manager =
            KeyringManager::new_insecure(TEST_APPLICATION, &insecure_keyring_path).unwrap();

        info!("TEST: secure_test_escaped_password_input");
        exec_test_escaped_password_input(&platform_keyring_manager);
        info!("TEST: secure_test_add_ascii_password");
        exec_test_add_ascii_password(&platform_keyring_manager);
        info!("TEST: secure_test_round_trip_ascii_password");
        exec_test_round_trip_ascii_password(&platform_keyring_manager);
        info!("TEST: secure_test_add_non_ascii_password");
        exec_test_add_non_ascii_password(&platform_keyring_manager);
        info!("TEST: secure_test_round_trip_non_ascii_password");
        exec_test_round_trip_non_ascii_password(&platform_keyring_manager);
        info!("TEST: secure_test_multiple");
        exec_test_multiple(&platform_keyring_manager);
        info!("TEST: insecure_test_escaped_password_input");
        exec_test_escaped_password_input(&insecure_keyring_manager);
        info!("TEST: insecure_test_add_ascii_password");
        exec_test_add_ascii_password(&insecure_keyring_manager);
        info!("TEST: insecure_test_round_trip_ascii_password");
        exec_test_round_trip_ascii_password(&insecure_keyring_manager);
        info!("TEST: insecure_test_add_non_ascii_password");
        exec_test_add_non_ascii_password(&insecure_keyring_manager);
        info!("TEST: insecure_test_round_trip_non_ascii_password");
        exec_test_round_trip_non_ascii_password(&insecure_keyring_manager);
        info!("TEST: insecure_test_multiple");
        exec_test_multiple(&insecure_keyring_manager);
    })
    .join()
    .unwrap();

    info!("Finished unit tests");
}
