#[cfg(target_os = "ios")]
use core_foundation_sys::base::OSStatus;
#[cfg(target_os = "linux")]
use secret_service::Error as SsError;
#[cfg(target_os = "macos")]
use security_framework::base::Error as SfError;
use std::error;
use std::fmt;
use std::string::FromUtf8Error;

pub type Result<T> = ::std::result::Result<T, KeyringError>;

#[derive(Debug)]
pub enum KeyringError {
    IoError(std::io::Error),
    #[cfg(target_os = "macos")]
    MacOsKeychainError(SfError),
    #[cfg(target_os = "ios")]
    IOSKeychainError(OSStatus),
    #[cfg(target_os = "linux")]
    SecretServiceError(SsError),
    #[cfg(target_os = "windows")]
    WindowsVaultError,
    #[cfg(target_os = "android")]
    AndroidKeystoreError(String),
    NoBackendFound,
    NoPasswordFound,
    Parse(FromUtf8Error),
    Generic(String),
}

impl fmt::Display for KeyringError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            KeyringError::IoError(ref err) => write!(f, "Keyring IO error: {}", err),
            #[cfg(target_os = "macos")]
            KeyringError::MacOsKeychainError(ref err) => {
                write!(f, "MacOS keychain Error: {}", err)
            }
            #[cfg(target_os = "ios")]
            KeyringError::IOSKeychainError(ref err) => {
                write!(f, "iOS keychain Error: {}", err)
            }
            #[cfg(target_os = "linux")]
            KeyringError::SecretServiceError(ref err) => write!(f, "Secret service error: {}", err),
            #[cfg(target_os = "windows")]
            KeyringError::WindowsVaultError => write!(f, "Windows vault error"),
            #[cfg(target_os = "android")]
            KeyringError::AndroidKeystoreError(s) => {
                write!(f, "Android keystore error: {}", s)
            }
            KeyringError::NoBackendFound => write!(f, "Keyring error: No Backend Found"),
            KeyringError::NoPasswordFound => write!(f, "Keyring error: No Password Found"),
            KeyringError::Parse(ref err) => write!(f, "Keyring parse error: {}", err),
            KeyringError::Generic(ref err) => write!(f, "Keyring generic error: {}", err),
        }
    }
}

impl error::Error for KeyringError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            KeyringError::IoError(ref err) => Some(err),
            #[cfg(target_os = "linux")]
            KeyringError::SecretServiceError(ref err) => Some(err),
            #[cfg(target_os = "macos")]
            KeyringError::MacOsKeychainError(ref err) => Some(err),
            _ => None,
        }
    }
}

impl From<std::io::Error> for KeyringError {
    fn from(err: std::io::Error) -> KeyringError {
        KeyringError::IoError(err)
    }
}

#[cfg(target_os = "linux")]
impl From<SsError> for KeyringError {
    fn from(err: SsError) -> KeyringError {
        KeyringError::SecretServiceError(err)
    }
}

#[cfg(target_os = "macos")]
impl From<SfError> for KeyringError {
    fn from(err: SfError) -> KeyringError {
        KeyringError::MacOsKeychainError(err)
    }
}

#[cfg(target_os = "ios")]
impl From<OSStatus> for KeyringError {
    fn from(err: OSStatus) -> KeyringError {
        KeyringError::IOSKeychainError(err)
    }
}

impl From<FromUtf8Error> for KeyringError {
    fn from(err: FromUtf8Error) -> KeyringError {
        KeyringError::Parse(err)
    }
}

impl From<String> for KeyringError {
    fn from(err: String) -> KeyringError {
        KeyringError::Generic(err)
    }
}
